(function () {
  const win = window
  const doc = document.documentElement

  doc.classList.remove('no-js')
  doc.classList.add('js')

  // Reveal animations
  if (document.body.classList.contains('has-animations')) {
    /* global ScrollReveal */
    const sr = window.sr = ScrollReveal()

    sr.reveal('.technology, .requirement', {
      duration: 600,
      distance: '20px',
      easing: 'cubic-bezier(0.5, -0.01, 0, 1.005)',
      origin: 'right',
      interval: 100
    })

    sr.reveal('.media-canvas', {
      duration: 600,
      scale: '.95',
      easing: 'cubic-bezier(0.5, -0.01, 0, 1.005)',
      viewFactor: 0.5
    })
  }

  // Wait that device mockup has loaded before displaying
  const deviceMockup = document.querySelector('.device-mockup')

  function deviceMockupLoaded () {
    deviceMockup.classList.add('has-loaded')
  }

  if (deviceMockup.complete) {
    deviceMockupLoaded()
  } else {
    deviceMockup.addEventListener('load', deviceMockupLoaded)
  }

  // Concepts title adjustment
  const conceptsSection = document.querySelector('.concepts')
  const conceptsTitle = conceptsSection.querySelector('.section-title')
  const firstConcept = document.querySelector('.concepts-inner')

  conceptsTitlePos()
  win.addEventListener('resize', conceptsTitlePos)

  function conceptsTitlePos () {
    let conceptsSectionLeft = conceptsSection.querySelector('.concepts-inner').getBoundingClientRect().left
    let firstConceptLeft = firstConcept.getBoundingClientRect().left
    let conceptsTitleOffset = parseInt(firstConceptLeft - conceptsSectionLeft)
    if (firstConceptLeft > conceptsSectionLeft) {
      conceptsTitle.style.marginLeft = `${conceptsTitleOffset}px`
    } else {
      conceptsTitle.style.marginLeft = 0
    }
  }

  // How works title adjustment
  const HowWorksSection = document.querySelector('.how-works')
  const HowWorksTitle = HowWorksSection.querySelector('.section-title')
  const firstHowWork = document.querySelector('.how-works-inner')

  HowWorksTitlePos()
  win.addEventListener('resize', HowWorksTitlePos)

  function HowWorksTitlePos () {
    let HowWorksSectionLeft = HowWorksSection.querySelector('.how-works-inner').getBoundingClientRect().left
    let firstHowWorkLeft = firstHowWork.getBoundingClientRect().left
    let HowWorksTitleOffset = parseInt(firstHowWorkLeft - HowWorksSectionLeft)
    if (firstHowWorkLeft > HowWorksSectionLeft) {
      HowWorksTitle.style.marginLeft = `${HowWorksTitleOffset}px`
    } else {
      HowWorksTitle.style.marginLeft = 0
    }
  }

  // Requeriments title adjustment
  const requerimentsSection = document.querySelector('.requeriments')
  const requerimentsTitle = requerimentsSection.querySelector('.section-title')
  const firstRequeriment = document.querySelector('.requeriments-inner')

  requerimentsTitlePos()
  win.addEventListener('resize', requerimentsTitlePos)

  function requerimentsTitlePos () {
    let requerimentsSectionLeft = requerimentsSection.querySelector('.requeriments-inner').getBoundingClientRect().left
    let firstRequerimentLeft = firstRequeriment.getBoundingClientRect().left
    let requerimentsTitleOffset = parseInt(firstRequerimentLeft - requerimentsSectionLeft)
    if (firstRequerimentLeft > requerimentsSectionLeft) {
      requerimentsTitle.style.marginLeft = `${requerimentsTitleOffset}px`
    } else {
      requerimentsTitle.style.marginLeft = 0
    }
  }

  // Technologies title adjustment
  const technologiesSection = document.querySelector('.technologies')
  const technologiesTitle = technologiesSection.querySelector('.section-title')
  const firstTechnology = document.querySelector('.technologies-inner')

  technologiesTitlePos()
  win.addEventListener('resize', technologiesTitlePos)

  function technologiesTitlePos () {
    let technologiesSectionLeft = technologiesSection.querySelector('.technologies-inner').getBoundingClientRect().left
    let firstTechnologyLeft = firstTechnology.getBoundingClientRect().left
    let technologiesTitleOffset = parseInt(firstTechnologyLeft - technologiesSectionLeft)
    if (firstTechnologyLeft > technologiesSectionLeft) {
      technologiesTitle.style.marginLeft = `${technologiesTitleOffset}px`
    } else {
      technologiesTitle.style.marginLeft = 0
    }
  }

  // Moving objects
  const movingObjects = document.querySelectorAll('.is-moving-object')

  // Throttling
  function throttle (func, milliseconds) {
    let lastEventTimestamp = null
    let limit = milliseconds

    return (...args) => {
      let now = Date.now()

      if (!lastEventTimestamp || now - lastEventTimestamp >= limit) {
        lastEventTimestamp = now
        func.apply(this, args)
      }
    }
  }

  // Init vars
  let mouseX = 0
  let mouseY = 0
  let scrollY = 0
  let coordinateX = 0
  let coordinateY = 0
  let winW = doc.clientWidth
  let winH = doc.clientHeight

  // Move Objects
  function moveObjects (e, object) {
    mouseX = e.pageX
    mouseY = e.pageY
    scrollY = win.scrollY
    coordinateX = (winW / 2) - mouseX
    coordinateY = (winH / 2) - (mouseY - scrollY)

    for (let i = 0; i < object.length; i++) {
      const translatingFactor = object[i].getAttribute('data-translating-factor') || 20
      const rotatingFactor = object[i].getAttribute('data-rotating-factor') || 20
      const perspective = object[i].getAttribute('data-perspective') || 500
      let tranformProperty = []

      if (object[i].classList.contains('is-translating')) {
        tranformProperty.push('translate(' + coordinateX / translatingFactor + 'px, ' + coordinateY / translatingFactor + 'px)')
      }

      if (object[i].classList.contains('is-rotating')) {
        tranformProperty.push('perspective(' + perspective + 'px) rotateY(' + -coordinateX / rotatingFactor + 'deg) rotateX(' + coordinateY / rotatingFactor + 'deg)')
      }

      if (object[i].classList.contains('is-translating') || object[i].classList.contains('is-rotating')) {
        tranformProperty = tranformProperty.join(' ')

        object[i].style.transform = tranformProperty
        object[i].style.transition = 'transform 1s ease-out'
        object[i].style.transformStyle = 'preserve-3d'
        object[i].style.backfaceVisibility = 'hidden'
      }
    }
  }

  // Call function with throttling
  if (movingObjects) {
    win.addEventListener('mousemove', throttle(
      function (e) {
        moveObjects(e, movingObjects)
      },
      150
    ))
  }
}())
